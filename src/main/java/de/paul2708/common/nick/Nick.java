package de.paul2708.common.nick;

import java.io.Serializable;

/**
 * Created by Paul on 28.05.2017.
 */
public class Nick implements Serializable {

    private String name;
    private Skin skin;

    public Nick(String name, Skin skin) {
        this.name = name;
        this.skin = skin;
    }

    public void setSkin(Skin skin) {
        this.skin = skin;
    }

    public String getName() {
        return name;
    }

    public Skin getSkin() {
        return skin;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Nick) {
            Nick nick = (Nick) obj;

            return nick.getName().equalsIgnoreCase(name);
        }

        return false;
    }
}
