package de.paul2708.common.nick;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Paul on 29.05.2017.
 */
public class Skin implements Serializable {

    private UUID uuid;
    private String value;
    private String signature;

    public Skin(UUID uuid, String value, String signature) {
        this.uuid = uuid;
        this.value = value;
        this.signature = signature;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getValue() {
        return value;
    }

    public String getSignature() {
        return signature;
    }
}
