package de.paul2708.common.network;

import de.jackwhite20.cascade.shared.protocol.Protocol;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.paul2708.common.packet.PacketManager;

/**
 * Created by Paul on 08.10.2016.
 */
public class PacketClientProtocol extends Protocol {

    public PacketClientProtocol(PacketListener... listeners) {
        for (PacketListener listener : listeners) {
            registerListener(listener);
        }

        for (Class<? extends Packet> clazz : PacketManager.packets) {
            registerPacket(clazz);
        }

    }
}
