package de.paul2708.common.network;

import de.jackwhite20.cascade.client.Client;
import de.jackwhite20.cascade.client.ClientFactory;
import de.jackwhite20.cascade.shared.Options;
import de.jackwhite20.cascade.shared.protocol.listener.PacketListener;
import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.session.Session;
import de.jackwhite20.cascade.shared.session.SessionListener;

import java.net.StandardSocketOptions;

/**
 * Created by Paul on 08.10.2016.
 */
public class PacketClient {

    private String host;

    private int port;

    private Client client;
    private PacketListener[] listeners;

    public PacketClient(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public void registerListeners(PacketListener... listeners) {
        this.listeners = listeners;
    }

    public void connect() {
        client = ClientFactory.create(host, port, new PacketClientProtocol(listeners), Options.of(StandardSocketOptions.TCP_NODELAY, true));

        client.addSessionListener(new SessionListener() {

            @Override
            public void onConnected(Session session) {
                System.out.println("PacketClient hat sich mit dem Server verbunden.");
            }

            @Override
            public void onDisconnected(Session session) {
                System.out.println("PacketClient hat die Verbindung getrennt.");
            }

            @Override
            public void onStarted() { }

            @Override
            public void onStopped() { }
        });
        client.connect();
    }

    public void send(Packet packet) {
        client.send(packet);
    }

    public Client getClient() {
        return client;
    }
}
