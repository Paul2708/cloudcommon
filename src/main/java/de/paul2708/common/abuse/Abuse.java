package de.paul2708.common.abuse;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Paul on 11.09.2016.
 */
public class Abuse implements Serializable {

    private AbuseAction action;
    private String staff;
    private String abuser;
    private String reason;
    private long timeStamp;
    private long lastTo;
    private UUID staffUuid;
    private UUID abuserUuid;

    public Abuse(AbuseAction action, String staff, String abuser, String reason, long timeStamp, long lastTo, UUID staffUuid, UUID abuserUuid) {
        this.action = action;
        this.staff = staff;
        this.abuser = abuser;
        this.reason = reason;
        this.timeStamp = timeStamp;
        this.lastTo = lastTo;
        this.staffUuid = staffUuid;
        this.abuserUuid = abuserUuid;
    }

    public AbuseAction getAction() {
        return action;
    }

    public String getStaff() {
        return staff;
    }

    public String getAbuser() {
        return abuser;
    }

    public String getReason() {
        return reason;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public long getLastTo() {
        return lastTo;
    }

    public UUID getStaffUuid() {
        return staffUuid;
    }

    public UUID getAbuserUuid() {
        return abuserUuid;
    }
}
