package de.paul2708.common.abuse;

import java.io.Serializable;

/**
 * Created by Paul on 11.09.2016.
 */
public enum AbuseAction implements Serializable {

    WARN("warn"),
    KICK("kick"),
    MUTE("mute"),
    BAN("ban"),
    UNBAN("unban"),
    UNMUTE("unmute"),
    ;

    private String data;

    AbuseAction(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public static AbuseAction getActionByName(String action) {
        for (AbuseAction all : AbuseAction.values()) {
            if (all.getData().equalsIgnoreCase(action)) {
                return all;
            }
        }

        return null;
    }
}
