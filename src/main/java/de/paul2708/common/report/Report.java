package de.paul2708.common.report;

import java.io.Serializable;

/**
 * Created by Paul on 16.08.2016.
 */
public class Report implements Serializable {

    private String reporterName;

    private String targetName;

    private String serverName;
    private Long time;

    private ReportCause cause;

    private int amount;

    public Report(String reporterName, String targetName, String serverName, Long time, ReportCause cause) {
        this.reporterName = reporterName;
        this.targetName = targetName;
        this.serverName = serverName;
        this.time = time;
        this.cause = cause;

        this.amount = 1;
    }

    public Report amount(int count) {
        this.amount = count;
        return this;
    }

    public String getReporterName() {
        return reporterName;
    }

    public String getTargetName() {
        return targetName;
    }

    public String getServerName() {
        return serverName;
    }

    public Long getTime() {
        return time;
    }

    public ReportCause getCause() {
        return cause;
    }

    public int getAmount() {
        return amount;
    }
}
