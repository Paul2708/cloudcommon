package de.paul2708.common.report;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Paul on 30.08.2016.
 */
public enum ReportCause implements Serializable {

    FLYHACK("Flyhack", 288, Collections.singletonList("Der Spieler kann unerlaubt fliegen oder höher springen.")),
    ANTI_KNOCKBACK("No-Knockback", 280, Arrays.asList("Der Spieler bekommt keinen Rückstoß oder", "er wird maximal reduziert.")),
    SPEEDHACK("Speedhack", 317, Arrays.asList("Der Spieler sprintet, während er sneakt oder", "rennt schneller als normal.")),
    KILLAURA("Killaura", 276, Arrays.asList("Der Spieler schlägt unkontrolliert nach", "Spielern und Mobs.")),
    USERNAME("Spielername", 145, Collections.singletonList("Der Spielername des Spielers ist anstößig.")),
    SKIN("Skin", 397, Collections.singletonList("Der Spieler hat einen anstößigen Skin.")),
    TEAMING("Teaming", 101, Arrays.asList("Der Spieler teamt mit einem Spieler oder", "Team, obwohl es verboten ist.")),
    TROLLING("Trolling", 385, Arrays.asList("Der Spieler trollt das Team. Dadurch wird der", "Spaß und Sinn des Spieles eingeschränkt.")),
    CHAT_ABUSE("Chat-Vergehen", 339, Collections.singletonList("Ein Spieler beleidigt, spammt oder droht.")),
    HACKING("Hacking", 323, Collections.singletonList("Der Spieler hackt mit einem anderen Hack.")),
    ;

    private String name;
    private int itemId;
    private List<String> description;

    ReportCause(String name, int itemId, List<String> description) {
        this.name = name;
        this.itemId = itemId;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public int getItemId() {
        return itemId;
    }

    public List<String> getDescription() {
        return description;
    }

    public static ReportCause getByName(String name) {
        for (ReportCause cause : ReportCause.values()) {
            if (cause.getName().equalsIgnoreCase(name)) {
                return cause;
            }
        }

        return null;
    }
}
