package de.paul2708.common.chatlog;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Paul on 03.09.2016.
 */
public class Chatlog implements Serializable {

    private String playerUuid;
    private String targetUuid;
    private String playerName;
    private String targetName;

    private ArrayList<String> chatLog;
    private Long time;

    public Chatlog(String playerUuid, String targetUuid, String playerName, String targetName, ArrayList<String> chatLog, Long time) {
        this.playerUuid = playerUuid;
        this.targetUuid = targetUuid;
        this.playerName = playerName;
        this.targetName = targetName;
        this.chatLog = chatLog;
        this.time = time;
    }

    public String getPlayerUuid() {
        return playerUuid;
    }

    public String getTargetUuid() {
        return targetUuid;
    }

    public String getPlayerName() {
        return playerName;
    }

    public String getTargetName() {
        return targetName;
    }

    public ArrayList<String> getChatLog() {
        return chatLog;
    }

    public Long getTime() {
        return time;
    }
}
