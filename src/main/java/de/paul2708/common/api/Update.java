package de.paul2708.common.api;

import de.paul2708.common.command.CommandType;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Paul on 29.12.2016.
 */
public class Update implements Serializable {

    private Type type;
    private Object[] arguments;

    public Update(Type type, Object... arguments) {
        this.type = type;
        this.arguments = arguments;
    }

    public Type getType() {
        return type;
    }

    public <T> T getArgument(int index) {
        return (T) type.getArguments()[index].cast(arguments[index]);
    }

    public enum Type {

        TOKENS(0, UUID.class, Integer.class),
        COMMAND(1, UUID.class, CommandType.class, String.class),
        SEND(2, UUID.class, String.class),
        MOTD(3, String.class, String.class),
        ;

        private int id;
        private Class<?>[] arguments;

        Type(int id, Class<?>... arguments) {
            this.id = id;
            this.arguments = arguments;
        }

        public int getId() {
            return id;
        }

        public Class<?>[] getArguments() {
            return arguments;
        }

        public static Type getById(int id) {
            for (Type information : values()) {
                if (information.id == id) {
                    return information;
                }
            }

            return null;
        }
    }
}
