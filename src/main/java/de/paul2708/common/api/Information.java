package de.paul2708.common.api;

import de.paul2708.common.nick.Nick;
import de.paul2708.common.rank.Rank;
import de.paul2708.common.server.ServerData;
import de.paul2708.common.stats.StatisticData;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Paul on 29.12.2016.
 */
public class Information implements Serializable {

    private Type type;
    private Object argument;
    private Object response;

    public Information(Type type, Object argument) {
        this.type = type;
        this.argument = argument;
    }

    public void setResponse(Object response) {
        this.response = response;
    }

    public Type getType() {
        return type;
    }

    public <T> T getArgument() {
        if (type.getArgument() == null) return null;
        return (T) type.getArgument().cast(argument);
    }

    public <T> T getResponse() {
        if (type.getResponse() == null) return null;
        return (T) type.getResponse().cast(response);
    }

    public enum Type {

        PLAYER_RANK(0, UUID.class, Rank.class),
        TOKENS(1, UUID.class, Integer.class),
        PLAY_TIME(2, UUID.class, Integer.class),
        ONLINE_PLAYERS(3, null, Integer.class),
        STATISTIC_DATA(4, StatisticData.class, StatisticData.class), // intern only
        SERVER_DATA(5, String.class, ServerData.class),
        AVAILABLE_SERVER(6, String.class, ServerData.class),
        INGAME_SERVER(7, String.class, ServerData[].class),
        NICK(8, UUID.class, Nick.class),
        ;

        private int id;
        private Class<?> argument;
        private Class<?> response;

        Type(int id, Class<?> argument, Class<?> response) {
            this.id = id;
            this.argument = argument;
            this.response = response;
        }

        public int getId() {
            return id;
        }

        public Class<?> getArgument() {
            return argument;
        }

        public Class<?> getResponse() {
            return response;
        }

        public <T> T get(String data) {
            return null;
        }

        public static Type getById(int id) {
            for (Type information : values()) {
                if (information.id == id) {
                    return information;
                }
            }

            return null;
        }
    }

}
