package de.paul2708.common.server;

import de.paul2708.common.game.GameState;

import java.io.Serializable;

/**
 * Created by Paul on 11.01.2017.
 */
public class ServerData implements Serializable {

    private String name;
    private int onlinePlayers, maxPlayers;
    private String motd;
    private GameState gameState;

    public ServerData(String name, int onlinePlayers, int maxPlayers, String motd, GameState gameState) {
        this.name = name;
        this.onlinePlayers = onlinePlayers;
        this.maxPlayers = maxPlayers;
        this.motd = motd;
        this.gameState = gameState;
    }

    public String getName() {
        return name;
    }

    public int getOnlinePlayers() {
        return onlinePlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public String getMotd() {
        return motd;
    }

    public GameState getGameState() {
        return gameState;
    }
}
