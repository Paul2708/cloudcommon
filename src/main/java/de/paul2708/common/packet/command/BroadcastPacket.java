package de.paul2708.common.packet.command;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 16.08.2016.
 */
@PacketInfo(2)
public class BroadcastPacket extends Packet {

    private String message;

    public BroadcastPacket() { }

    public BroadcastPacket(String message) {
        this.message = message;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.message = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, message);
    }

    public String getMessage() {
        return message;
    }
}
