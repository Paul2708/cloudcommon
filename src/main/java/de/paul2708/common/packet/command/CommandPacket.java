package de.paul2708.common.packet.command;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.08.2016.
 */
@PacketInfo(3)
public class CommandPacket extends Packet {

    private String name;
    private String command;
    private String[] args;

    public CommandPacket() { }

    public CommandPacket(String name, String command, String[] args) {
        this.name = name;
        this.command = command;
        this.args = args;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.command = readString(byteBuf);
        this.args = readArrayString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeString(byteBuf, command);
        writeArrayString(byteBuf, args);
    }

    public String getName() {
        return name;
    }

    public String getCommand() {
        return command;
    }

    public String[] getArguments() {
        return args;
    }
}
