package de.paul2708.common.packet.stats;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.stats.StatisticData;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 16.10.2016.
 */
@PacketInfo(29)
public class StatsRequestPacket extends Packet {

    private int id;

    private StatisticData statisticData;

    public StatsRequestPacket() { }

    public StatsRequestPacket(int id, StatisticData statistic) {
        this.id = id;
        this.statisticData = statistic;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.statisticData = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        writeObject(byteBuf, statisticData);
    }

    public int getId() {
        return id;
    }

    public StatisticData getStatisticData() {
        return statisticData;
    }
}
