package de.paul2708.common.packet.stats;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.stats.GameStatistic;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 16.10.2016.
 */
@PacketInfo(28)
public class StatsRegisterPacket extends Packet {

    private GameStatistic gameStatistic;

    public StatsRegisterPacket() { }

    public StatsRegisterPacket(GameStatistic gameStatistic) {
        this.gameStatistic = gameStatistic;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.gameStatistic = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeObject(byteBuf, gameStatistic);
    }

    public GameStatistic getGameStatistic() {
        return gameStatistic;
    }
}
