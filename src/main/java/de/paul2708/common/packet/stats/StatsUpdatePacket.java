package de.paul2708.common.packet.stats;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 21.10.2016.
 */
@PacketInfo(30)
public class StatsUpdatePacket extends Packet {

    private String game;
    private UUID uuid;
    private String key;
    private Object value;

    public StatsUpdatePacket() { }

    public StatsUpdatePacket(String game, UUID uuid, String key, Object value) {
        this.game = game;
        this.uuid = uuid;
        this.key = key;
        this.value = value;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.game = readString(byteBuf);
        this.uuid = readUUID(byteBuf);
        this.key = readString(byteBuf);
        this.value = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, game);
        writeUUID(byteBuf, uuid);
        writeString(byteBuf, key);
        writeObject(byteBuf, value);
    }

    public String getGame() {
        return game;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }
}
