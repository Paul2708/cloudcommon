package de.paul2708.common.packet.stats;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.stats.StatisticData;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 24.10.2016.
 */
@PacketInfo(31)
public class StatsPushPacket extends Packet {

    private StatisticData data;

    public StatsPushPacket() { }

    public StatsPushPacket(StatisticData data) {
        this.data = data;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.data = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeObject(byteBuf, data);
    }

    public StatisticData getData() {
        return data;
    }
}
