package de.paul2708.common.packet.stats;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.stats.StatisticData;
import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Paul on 29.10.2016.
 */
@PacketInfo(32)
public class StatsCommandPacket extends Packet {

    private UUID uuid;
    private StatisticData data;
    private ArrayList<String> result;

    public StatsCommandPacket() { }

    public StatsCommandPacket(UUID uuid, StatisticData data, ArrayList<String> result) {
        this.uuid = uuid;
        this.data = data;
        this.result = result;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readUUID(byteBuf);
        this.data = readObject(byteBuf);
        this.result = readArrayListString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeUUID(byteBuf, uuid);
        writeObject(byteBuf, data);
        writeArrayListString(byteBuf, result);
    }

    public UUID getUuid() {
        return uuid;
    }

    public StatisticData getData() {
        return data;
    }

    public ArrayList<String> getResult() {
        return result;
    }
}
