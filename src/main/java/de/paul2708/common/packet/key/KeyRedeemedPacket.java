package de.paul2708.common.packet.key;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 01.01.2017.
 */
@PacketInfo(38)
public class KeyRedeemedPacket extends Packet {

    private UUID uuid;

    public KeyRedeemedPacket() { }

    public KeyRedeemedPacket(UUID uuid) {
        this.uuid = uuid;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readUUID(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
       writeUUID(byteBuf, uuid);
    }

    public UUID getUuid() {
        return uuid;
    }
}
