package de.paul2708.common.packet.rank;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.rank.Rank;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.08.2016.
 */
@PacketInfo(16)
public class RankUpdatePacket extends Packet {

    private String uuid;
    private Rank from;
    private Rank to;
    private long duration;

    public RankUpdatePacket() { }

    public RankUpdatePacket(String uuid, Rank from, Rank to, long duration) {
        this.uuid = uuid;
        this.from = from;
        this.to = to;
        this.duration = duration;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readString(byteBuf);
        this.from = readObject(byteBuf);
        this.to = readObject(byteBuf);
        this.duration = byteBuf.readLong();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, uuid);
        writeObject(byteBuf, from);
        writeObject(byteBuf, to);
        byteBuf.writeLong(duration);
    }

    public String getUuid() {
        return uuid;
    }

    public Rank getFrom() {
        return from;
    }

    public Rank getTo() {
        return to;
    }

    public long getDuration() {
        return duration;
    }
}
