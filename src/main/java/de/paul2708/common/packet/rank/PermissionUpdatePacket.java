package de.paul2708.common.packet.rank;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.rank.Rank;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 15.08.2016.
 */
@PacketInfo(15)
public class PermissionUpdatePacket extends Packet {

    private Rank rank;

    public PermissionUpdatePacket() { }

    public PermissionUpdatePacket(Rank rank) {
        this.rank = rank;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.rank = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeObject(byteBuf, rank);
    }

    public Rank getRank() {
        return rank;
    }
}
