package de.paul2708.common.packet.rank;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.rank.Rank;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.08.2016.
 */
@PacketInfo(14)
public class GetPlayerRankPacket extends Packet {

    private String uuid;
    private Rank rank;

    public GetPlayerRankPacket() { }

    public GetPlayerRankPacket(String uuid, Rank rank) {
        this.uuid = uuid;
        this.rank = rank;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readString(byteBuf);
        this.rank = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, uuid);
        writeObject(byteBuf, rank);
    }

    public String getUuid() {
        return uuid;
    }

    public Rank getRank() {
        return rank;
    }
}
