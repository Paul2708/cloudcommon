package de.paul2708.common.packet.api;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.api.Update;
import de.paul2708.common.util.Util;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 26.12.2016.
 */
@PacketInfo(37)
public class ApiUpdatePacket extends Packet {

    private int id;

    private Update[] updates;

    public ApiUpdatePacket() { }

    public ApiUpdatePacket(int id, Update[] updates) {
        this.id = id;
        this.updates = updates;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.updates = Util.readUpdateArray(this, byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        Util.writeUpdateArray(this, byteBuf, updates);
    }

    public int getId() {
        return id;
    }

    public Update[] getUpdates() {
        return updates;
    }
}
