package de.paul2708.common.packet.api;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 16.10.2016.
 */
@PacketInfo(27)
public class ApiOnlinePacket extends Packet {

    private int id;

    private int value;

    public ApiOnlinePacket() { }

    public ApiOnlinePacket(int id, int value) {
        this.id = id;
        this.value = value;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.value = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        byteBuf.writeInt(value);
    }

    public int getId() {
        return id;
    }

    public int getValue() {
        return value;
    }
}
