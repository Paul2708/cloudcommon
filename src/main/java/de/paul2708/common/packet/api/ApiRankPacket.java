package de.paul2708.common.packet.api;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.rank.Rank;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 16.10.2016.
 */
@PacketInfo(24)
public class ApiRankPacket extends Packet {

    private int id;
    private UUID uuid;
    private String name;
    private Rank rank;

    public ApiRankPacket() { }

    public ApiRankPacket(int id, UUID uuid, String name, Rank rank) {
        this.id = id;
        this.uuid = uuid;
        this.name = name;
        this.rank = rank;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.uuid = readUUID(byteBuf);
        this.name = readString(byteBuf);
        this.rank = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        writeUUID(byteBuf, uuid);
        writeString(byteBuf, name);
        writeObject(byteBuf, rank);
    }

    public int getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public Rank getRank() {
        return rank;
    }
}
