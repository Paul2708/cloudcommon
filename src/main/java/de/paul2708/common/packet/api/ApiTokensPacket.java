package de.paul2708.common.packet.api;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 16.10.2016.
 */
@PacketInfo(25)
public class ApiTokensPacket extends Packet {

    private int id;

    private UUID uuid;
    private String name;
    private String operator;
    private int value;

    public ApiTokensPacket() { }

    public ApiTokensPacket(int id, UUID uuid, String name, String operator, int value) {
        this.id = id;
        this.uuid = uuid;
        this.name = name;
        this.operator = operator;
        this.value = value;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.uuid = readUUID(byteBuf);
        this.name = readString(byteBuf);
        this.operator = readString(byteBuf);
        this.value = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        writeUUID(byteBuf, uuid);
        writeString(byteBuf, name);
        writeString(byteBuf, operator);
        byteBuf.writeInt(value);
    }

    public int getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public String getOperator() {
        return operator;
    }

    public int getValue() {
        return value;
    }
}
