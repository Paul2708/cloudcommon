package de.paul2708.common.packet.api;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 16.10.2016.
 */
@PacketInfo(26)
public class ApiPlayTimePacket extends Packet {

    private int id;

    private UUID uuid;
    private String name;
    private int value;

    public ApiPlayTimePacket() { }

    public ApiPlayTimePacket(int id, UUID uuid, String name, int value) {
        this.id = id;
        this.uuid = uuid;
        this.name = name;
        this.value = value;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.uuid = readUUID(byteBuf);
        this.name = readString(byteBuf);
        this.value = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        writeUUID(byteBuf, uuid);
        writeString(byteBuf, name);
        byteBuf.writeInt(value);
    }

    public int getId() {
        return id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
