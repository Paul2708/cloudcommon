package de.paul2708.common.packet.api;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.api.Information;
import de.paul2708.common.util.Util;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 26.12.2016.
 */
@PacketInfo(36)
public class ApiInformationPacket extends Packet {

    private int id;

    private String reason;
    private Information[] information;

    public ApiInformationPacket() { }

    public ApiInformationPacket(int id, String reason, Information[] information) {
        this.id = id;
        this.reason = reason;
        this.information = information;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.id = byteBuf.readInt();
        this.reason = readString(byteBuf);
        this.information = Util.readInformationArray(this, byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        writeString(byteBuf, reason);
        Util.writeInformationArray(this, byteBuf, information);
    }

    public int getId() {
        return id;
    }

    public String getReason() {
        return reason;
    }

    public Information[] getInformation() {
        return information;
    }
}
