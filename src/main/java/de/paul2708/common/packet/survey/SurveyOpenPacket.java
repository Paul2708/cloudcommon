package de.paul2708.common.packet.survey;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.survey.Question;
import de.paul2708.common.util.Util;
import io.netty.buffer.ByteBuf;

import java.util.List;
import java.util.UUID;

/**
 * Created by Paul on 03.09.2016.
 */
@PacketInfo(48)
public class SurveyOpenPacket extends Packet {

    private UUID uuid;
    private List<Question> questions;

    public SurveyOpenPacket() { }

    public SurveyOpenPacket(UUID uuid, List<Question> questions) {
        this.uuid = uuid;
        this.questions = questions;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readUUID(byteBuf);
        this.questions = Util.readList(this, byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeUUID(byteBuf, uuid);
        Util.writeList(this, byteBuf, questions);
    }

    public UUID getUuid() {
        return uuid;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}
