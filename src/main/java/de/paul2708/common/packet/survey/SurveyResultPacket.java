package de.paul2708.common.packet.survey;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.survey.Result;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 03.09.2016.
 */
@PacketInfo(49)
public class SurveyResultPacket extends Packet {

    private UUID uuid;
    private Result result;

    public SurveyResultPacket() { }

    public SurveyResultPacket(UUID uuid, Result result) {
        this.uuid = uuid;
        this.result = result;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readUUID(byteBuf);
        this.result = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeUUID(byteBuf, uuid);
        writeObject(byteBuf, result);
    }

    public UUID getUuid() {
        return uuid;
    }

    public Result getResult() {
        return result;
    }
}
