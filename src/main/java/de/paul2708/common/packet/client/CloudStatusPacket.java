package de.paul2708.common.packet.client;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 07.08.2016.
 */
@PacketInfo(1)
public class CloudStatusPacket extends Packet {

    private String status;

    public CloudStatusPacket() { }

    public CloudStatusPacket(String status) {
        this.status = status;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.status = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, status);
    }

    public String getStatus() {
        return status;
    }
}
