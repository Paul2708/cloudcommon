package de.paul2708.common.packet.client;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.packet.ClientType;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.08.2016.
 */
@PacketInfo()
public class ClientLoginPacket extends Packet {

    private int id;
    private String name;

    private ClientType type;

    public ClientLoginPacket() {

    }

    public ClientLoginPacket(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {

        this.id = byteBuf.readInt();
        this.name = readString(byteBuf);

        this.type = ClientType.getClientType(id);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(id);
        writeString(byteBuf, name);
    }

    public ClientType getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
