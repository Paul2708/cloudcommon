package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 07.08.2016.
 */
@PacketInfo(11)
public class PlayerLoginPacket extends Packet {

    private String name;
    private String uuid;
    private String ip;

    public PlayerLoginPacket() { }

    public PlayerLoginPacket(String name, String uuid, String ip) {
        this.name = name;
        this.uuid = uuid;
        this.ip = ip;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.uuid = readString(byteBuf);
        this.ip = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeString(byteBuf, uuid);
        writeString(byteBuf, ip);
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public String getIp() {
        return ip;
    }
}
