package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.08.2016.
 */
@PacketInfo(13)
public class PlayerMessagePacket extends Packet {

    private String name;
    private String[] messages;

    public PlayerMessagePacket() { }

    public PlayerMessagePacket(String name, String... messages) {
        this.name = name;
        this.messages = messages;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.messages = readArrayString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeArrayString(byteBuf, messages);
    }

    public String getName() {
        return name;
    }

    public String[] getMessages() {
        return messages;
    }
}
