package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 05.09.2016.
 */
@PacketInfo(10)
public class PlayerKickPacket extends Packet {

    private String name;
    private String reason;

    public PlayerKickPacket() { }

    public PlayerKickPacket(String name, String reason) {
        this.name = name;
        this.reason = reason;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.reason = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeString(byteBuf, reason);
    }

    public String getName() {
        return name;
    }

    public String getReason() {
        return reason;
    }
}
