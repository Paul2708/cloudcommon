package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 17.08.2016.
 */
@PacketInfo(8)
public class PlayerChangeServerPacket extends Packet {

    private String uuid;
    private String serverName;

    public PlayerChangeServerPacket() { }

    public PlayerChangeServerPacket(String uuid, String serverName) {
        this.uuid = uuid;
        this.serverName = serverName;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readString(byteBuf);
        this.serverName = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, uuid);
        writeString(byteBuf, serverName);
    }

    public String getUuid() {
        return uuid;
    }

    public String getServerName() {
        return serverName;
    }
}
