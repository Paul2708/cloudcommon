package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.rank.Rank;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.09.2016.
 */
@PacketInfo(9)
public class PlayerImageMessagePacket extends Packet {

    private String name;
    private String uuid;
    private Rank rank;
    private String server;

    public PlayerImageMessagePacket() { }

    public PlayerImageMessagePacket(String name, String uuid, Rank rank, String server) {
        this.name = name;
        this.uuid = uuid;
        this.rank = rank;
        this.server = server;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.uuid = readString(byteBuf);
        this.rank = readObject(byteBuf);
        this.server = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeString(byteBuf, uuid);
        writeObject(byteBuf, rank);
        writeString(byteBuf, server);
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public Rank getRank() {
        return rank;
    }

    public String getServer() {
        return server;
    }
}
