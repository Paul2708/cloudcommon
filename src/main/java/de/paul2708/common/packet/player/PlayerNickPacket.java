package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.nick.Nick;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 05.11.2016.
 */
@PacketInfo(47)
public class PlayerNickPacket extends Packet {

    private UUID uuid;
    private Nick nick;

    public PlayerNickPacket() { }

    public PlayerNickPacket(UUID uuid, Nick nick) {
        this.uuid = uuid;
        this.nick = nick;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readUUID(byteBuf);
        this.nick = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeUUID(byteBuf, uuid);
        writeObject(byteBuf, nick);
    }

    public UUID getUuid() {
        return uuid;
    }

    public Nick getNick() {
        return nick;
    }
}
