package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 07.08.2016.
 */
@PacketInfo(12)
public class PlayerLogoutPacket extends Packet {

    private String uuid;

    public PlayerLogoutPacket() { }

    public PlayerLogoutPacket(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, uuid);
    }

    public String getUuid() {
        return uuid;
    }
}
