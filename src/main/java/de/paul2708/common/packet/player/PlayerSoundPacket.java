package de.paul2708.common.packet.player;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 05.11.2016.
 */
@PacketInfo((byte) 33)
public class PlayerSoundPacket extends Packet {

    private UUID uuid;
    private String sound;

    public PlayerSoundPacket() { }

    public PlayerSoundPacket(UUID uuid, String sound) {
        this.uuid = uuid;
        this.sound = sound;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.uuid = readUUID(byteBuf);
        this.sound = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeUUID(byteBuf, uuid);
        writeString(byteBuf, sound);
    }

    public UUID getUuid() {
        return uuid;
    }

    public String getSound() {
        return sound;
    }
}
