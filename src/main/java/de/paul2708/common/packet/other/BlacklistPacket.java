package de.paul2708.common.packet.other;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 06.09.2016.
 */
@PacketInfo(5)
public class BlacklistPacket extends Packet {

    private String operation;
    private String name;

    public BlacklistPacket() { }

    public BlacklistPacket(String operation, String name) {
        this.operation = operation;
        this.name = name;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.operation = readString(byteBuf);
        this.name = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, operation);
        writeString(byteBuf, name);
    }

    public String getOperation() {
        return operation;
    }

    public String getName() {
        return name;
    }
}
