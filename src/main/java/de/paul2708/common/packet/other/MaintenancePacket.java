package de.paul2708.common.packet.other;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 07.09.2016.
 */
@PacketInfo(7)
public class MaintenancePacket extends Packet {

    private String operation;
    private String reason;
    private long time;

    public MaintenancePacket() { }

    public MaintenancePacket(String operation, String reason, long time) {
        this.operation = operation;
        this.reason = reason;
        this.time = time;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.operation = readString(byteBuf);
        this.reason = readString(byteBuf);
        this.time = byteBuf.readLong();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, operation);
        writeString(byteBuf, reason);
        byteBuf.writeLong(time);
    }

    public String getOperation() {
        return operation;
    }

    public String getReason() {
        return reason;
    }

    public long getTime() {
        return time;
    }
}
