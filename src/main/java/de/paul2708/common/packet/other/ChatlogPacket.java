package de.paul2708.common.packet.other;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.chatlog.Chatlog;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 03.09.2016.
 */
@PacketInfo(6)
public class ChatlogPacket extends Packet {

    private Chatlog chatlog;

    public ChatlogPacket() { }

    public ChatlogPacket(Chatlog chatlog) {
        this.chatlog = chatlog;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.chatlog = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeObject(byteBuf, chatlog);
    }

    public Chatlog getChatlog() {
        return chatlog;
    }
}
