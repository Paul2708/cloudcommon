package de.paul2708.common.packet.other;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.abuse.Abuse;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 02.10.2016.
 */
@PacketInfo(4)
public class AbusePacket extends Packet {

    private String action;
    private Abuse abuse;

    public AbusePacket() { }

    public AbusePacket(String action, Abuse abuse) {
        this.action = action;
        this.abuse = abuse;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.action = readString(byteBuf);
        this.abuse = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, action);
        writeObject(byteBuf, abuse);
    }

    public String getAction() {
        return action;
    }

    public Abuse getAbuse() {
        return abuse;
    }
}
