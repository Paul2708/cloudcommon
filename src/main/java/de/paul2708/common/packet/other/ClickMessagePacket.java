package de.paul2708.common.packet.other;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 08.01.2017.
 */
@PacketInfo(39)
public class ClickMessagePacket extends Packet {

    private UUID to;
    private String message;
    private String hoverText;
    private String command;
    private String suggest;

    public ClickMessagePacket() { }

    public ClickMessagePacket(UUID to, String message, String hoverText, String command, String suggest) {
        this.to = to;
        this.message = message;
        this.hoverText = hoverText;
        this.command = command;
        this.suggest = suggest;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.to = readUUID(byteBuf);
        this.message = readString(byteBuf);
        this.hoverText = readString(byteBuf);
        this.command = readString(byteBuf);
        this.suggest = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeUUID(byteBuf, to);
        writeString(byteBuf, message);
        writeString(byteBuf, hoverText);
        writeString(byteBuf, command);
        writeString(byteBuf, suggest);
    }

    public UUID getTo() {
        return to;
    }

    public String getMessage() {
        return message;
    }

    public String getHoverText() {
        return hoverText;
    }

    public String getCommand() {
        return command;
    }

    public String getSuggest() {
        return suggest;
    }
}
