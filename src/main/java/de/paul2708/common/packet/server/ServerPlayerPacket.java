package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

import java.util.UUID;

/**
 * Created by Paul on 25.08.2016.
 */
@PacketInfo(40)
public class ServerPlayerPacket extends Packet {

    private String server;
    private String operation;
    private UUID uuid;

    public ServerPlayerPacket() { }

    public ServerPlayerPacket(String server, String operation, UUID uuid) {
        this.server = server;
        this.operation = operation;
        this.uuid = uuid;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.server = readString(byteBuf);
        this.operation = readString(byteBuf);
        this.uuid = readUUID(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, server);
        writeString(byteBuf, operation);
        writeUUID(byteBuf, uuid);
    }

    public String getServer() {
        return server;
    }

    public String getOperation() {
        return operation;
    }

    public UUID getUuid() {
        return uuid;
    }
}
