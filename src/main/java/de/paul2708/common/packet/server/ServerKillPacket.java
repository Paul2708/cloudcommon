package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 02.03.2017.
 */
@PacketInfo(45)
public class ServerKillPacket extends Packet {

    private String server;

    public ServerKillPacket() { }

    public ServerKillPacket(String server) {
        this.server = server;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.server = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, server);
    }

    public String getServer() {
        return server;
    }
}
