package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.abuse.Abuse;
import de.paul2708.common.util.Util;
import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Paul on 06.09.2016.
 */
@PacketInfo(22)
public class BungeeRegistrationPacket extends Packet {

    private String motd;
    private int maxPlayers;
    private int teamLevel;
    private ArrayList<String> blackList;
    private boolean maintenance;
    private String maintenanceReason;
    private ArrayList<UUID> teamMember;
    private ArrayList<UUID> joinMember;
    private ArrayList<Abuse> abuses;

    public BungeeRegistrationPacket() { }

    public BungeeRegistrationPacket(String motd, int maxPlayers, int teamLevel, ArrayList<String> blackList,
                                    boolean maintenance, String maintenanceReason,
                                    ArrayList<UUID> teamMember,
                                    ArrayList<UUID> joinMember,
                                    ArrayList<Abuse> abuses) {
        this.motd = motd;
        this.maxPlayers = maxPlayers;
        this.teamLevel = teamLevel;
        this.blackList = blackList;
        this.maintenance = maintenance;
        this.maintenanceReason = maintenanceReason;
        this.teamMember = teamMember;
        this.joinMember = joinMember;
        this.abuses = abuses;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.motd = readString(byteBuf);
        this.maxPlayers = byteBuf.readInt();
        this.teamLevel = byteBuf.readInt();
        this.blackList = readArrayListString(byteBuf);
        this.maintenance = byteBuf.readBoolean();
        this.maintenanceReason = readString(byteBuf);
        this.teamMember = Util.readListUUID(this, byteBuf);
        this.joinMember = Util.readListUUID(this, byteBuf);
        this.abuses = Util.readList(this, byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, motd);
        byteBuf.writeInt(maxPlayers);
        byteBuf.writeInt(teamLevel);
        writeArrayListString(byteBuf, blackList);
        byteBuf.writeBoolean(maintenance);
        writeString(byteBuf, maintenanceReason);
        Util.writeListUUID(this, byteBuf, teamMember);
        Util.writeListUUID(this, byteBuf, joinMember);
        Util.writeList(this, byteBuf, abuses);
    }

    public String getMotd() {
        return motd;
    }

    public Integer getMaxPlayers() {
        return maxPlayers;
    }

    public int getTeamLevel() {
        return teamLevel;
    }

    public ArrayList<String> getBlackList() {
        return blackList;
    }

    public Boolean isMaintenance() {
        return maintenance;
    }

    public String getMaintenanceReason() {
        return maintenanceReason;
    }

    public ArrayList<UUID> getTeamMember() {
        return teamMember;
    }

    public ArrayList<UUID> getJoinMember() {
        return joinMember;
    }

    public ArrayList<Abuse> getAbuses() {
        return abuses;
    }
}
