package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 25.08.2016.
 */
@PacketInfo(21)
public class AddServerPacket extends Packet {

    private String name;
    private String ip;
    private int port;

    public AddServerPacket() { }

    public AddServerPacket(String name, String ip, int port) {
        this.name = name;
        this.ip = ip;
        this.port = port;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.ip = readString(byteBuf);
        this.port = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeString(byteBuf, ip);
        byteBuf.writeInt(port);
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

}
