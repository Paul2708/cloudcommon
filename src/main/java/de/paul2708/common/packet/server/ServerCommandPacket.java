package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 20.05.2017.
 */
@PacketInfo(46)
public class ServerCommandPacket extends Packet {

    private String server;
    private String command;

    public ServerCommandPacket() { }

    public ServerCommandPacket(String server, String command) {
        this.server = server;
        this.command = command;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.server = readString(byteBuf);
        this.command = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, server);
        writeString(byteBuf, command);
    }

    public String getServer() {
        return server;
    }

    public String getCommand() {
        return command;
    }
}
