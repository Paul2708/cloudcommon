package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 02.09.2016.
 */
@PacketInfo(23)
public class MotdPacket extends Packet {

    private String motd;

    public MotdPacket() { }

    public MotdPacket(String motd) {
        this.motd = motd;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.motd = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, motd);
    }

    public String getMotd() {
        return motd;
    }
}
