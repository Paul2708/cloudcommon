package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 08.01.2017.
 */
@PacketInfo(41)
public class ServerRegistrationPacket extends Packet {

    private String name;
    private String ip;
    private int port;
    private boolean listed;
    private boolean gameServer;
    private String motd;
    private int maxPlayers;

    public ServerRegistrationPacket() { }

    public ServerRegistrationPacket(String name, String ip, int port, boolean listed, boolean gameServer, String motd, int maxPlayers) {
        this.name = name;
        this.ip = ip;
        this.port = port;
        this.listed = listed;
        this.gameServer = gameServer;
        this.motd = motd;
        this.maxPlayers = maxPlayers;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.ip = readString(byteBuf);
        this.port = byteBuf.readInt();
        this.listed = byteBuf.readBoolean();
        this.gameServer = byteBuf.readBoolean();
        this.motd = readString(byteBuf);
        this.maxPlayers = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        writeString(byteBuf, ip);
        byteBuf.writeInt(port);
        byteBuf.writeBoolean(listed);
        byteBuf.writeBoolean(gameServer);
        writeString(byteBuf, motd);
        byteBuf.writeInt(maxPlayers);
    }

    public String getName() {
        return name;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public boolean isListed() {
        return listed;
    }

    public boolean isGameServer() {
        return gameServer;
    }

    public String getMotd() {
        return motd;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }
}
