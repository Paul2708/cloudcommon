package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 25.08.2016.
 */
@PacketInfo(42)
public class ServerStatePacket extends Packet {

    private String server;
    private int state;

    public ServerStatePacket() { }

    public ServerStatePacket(String server, int state) {
        this.server = server;
        this.state = state;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.server = readString(byteBuf);
        this.state = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, server);
        byteBuf.writeInt(state);
    }

    public String getServer() {
        return server;
    }

    public int getState() {
        return state;
    }
}
