package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 25.08.2016.
 */
@PacketInfo(43)
public class ServerJoinPacket extends Packet {

    private String server;
    private boolean canJoin;

    public ServerJoinPacket() { }

    public ServerJoinPacket(String server, boolean canJoin) {
        this.server = server;
        this.canJoin = canJoin;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.server = readString(byteBuf);
        this.canJoin = byteBuf.readBoolean();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, server);
        byteBuf.writeBoolean(canJoin);
    }

    public String getServer() {
        return server;
    }

    public boolean canJoin() {
        return canJoin;
    }
}
