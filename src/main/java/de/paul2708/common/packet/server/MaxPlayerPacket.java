package de.paul2708.common.packet.server;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 02.09.2016.
 */
@PacketInfo(44)
public class MaxPlayerPacket extends Packet {

    private int maxPlayers;

    public MaxPlayerPacket() { }

    public MaxPlayerPacket(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.maxPlayers = byteBuf.readInt();
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        byteBuf.writeInt(maxPlayers);
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }
}
