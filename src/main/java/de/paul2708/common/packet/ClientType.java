package de.paul2708.common.packet;

/**
 * Created by Paul on 07.10.2016.
 */
public enum ClientType {

    BUNGEE(0, "BungeeCord"),
    SPIGOT(1, "SpigotServer"),
    API(2, "CloudAPI"),
    ;

    private int id;
    private String name;

    ClientType(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public static ClientType getClientType(int id) {
        for (ClientType type : values()) {
            if (type.getId() == id) {
                return type;
            }
        }

        return null;
    }
}
