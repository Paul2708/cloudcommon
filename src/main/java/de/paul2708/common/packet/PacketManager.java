package de.paul2708.common.packet;

import de.paul2708.common.packet.api.*;
import de.paul2708.common.packet.client.ClientLoginPacket;
import de.paul2708.common.packet.client.CloudStatusPacket;
import de.paul2708.common.packet.command.BroadcastPacket;
import de.paul2708.common.packet.command.CommandPacket;
import de.paul2708.common.packet.key.KeyRedeemedPacket;
import de.paul2708.common.packet.other.*;
import de.paul2708.common.packet.player.*;
import de.paul2708.common.packet.rank.GetPlayerRankPacket;
import de.paul2708.common.packet.rank.PermissionUpdatePacket;
import de.paul2708.common.packet.rank.RankUpdatePacket;
import de.paul2708.common.packet.report.CreateReportPacket;
import de.paul2708.common.packet.report.ListReportPacket;
import de.paul2708.common.packet.report.RemoveReportPacket;
import de.paul2708.common.packet.report.RequestReportPacket;
import de.paul2708.common.packet.server.*;
import de.paul2708.common.packet.stats.*;
import de.paul2708.common.packet.survey.SurveyResultPacket;
import de.paul2708.common.packet.survey.SurveyOpenPacket;

/**
 * Created by Paul on 07.10.2016.
 */
public class PacketManager {

    // TODO: Change String uuid to UUID

    // Total: 50
    public static Class[] packets = new Class[] {
            ClientLoginPacket.class,
            CloudStatusPacket.class,
            BroadcastPacket.class,
            CommandPacket.class,
            CreateReportPacket.class,
            ListReportPacket.class,
            RemoveReportPacket.class,
            RequestReportPacket.class,
            AbusePacket.class,
            BlacklistPacket.class,
            ChatlogPacket.class,
            MaintenancePacket.class,
            PlayerChangeServerPacket.class,
            PlayerImageMessagePacket.class,
            PlayerKickPacket.class,
            PlayerLoginPacket.class,
            PlayerLogoutPacket.class,
            PlayerMessagePacket.class,
            GetPlayerRankPacket.class,
            PermissionUpdatePacket.class,
            RankUpdatePacket.class,
            AddServerPacket.class,
            BungeeRegistrationPacket.class,
            MotdPacket.class,
            ApiRankPacket.class,
            ApiTokensPacket.class,
            ApiPlayTimePacket.class,
            ApiOnlinePacket.class,
            StatsRegisterPacket.class,
            StatsRequestPacket.class,
            StatsUpdatePacket.class,
            StatsPushPacket.class,
            StatsCommandPacket.class,
            PlayerSoundPacket.class,
            ApiInformationPacket.class,
            ApiUpdatePacket.class,
            KeyRedeemedPacket.class,
            ClickMessagePacket.class,
            ServerPlayerPacket.class,
            ServerRegistrationPacket.class,
            ServerStatePacket.class,
            ServerJoinPacket.class,
            MaxPlayerPacket.class,
            ServerKillPacket.class,
            ServerCommandPacket.class,
            PlayerNickPacket.class,
            SurveyOpenPacket.class,
            SurveyResultPacket.class
    };
}
