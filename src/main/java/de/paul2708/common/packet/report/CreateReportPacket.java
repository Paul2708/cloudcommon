package de.paul2708.common.packet.report;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.report.Report;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 16.08.2016.
 */
@PacketInfo(17)
public class CreateReportPacket extends Packet {

    private Report report;

    public CreateReportPacket() { }

    public CreateReportPacket(Report report) {
        this.report = report;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.report = readObject(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeObject(byteBuf, report);
    }

    public Report getReport() {
        return report;
    }
}
