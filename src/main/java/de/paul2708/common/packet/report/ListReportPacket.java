package de.paul2708.common.packet.report;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import de.paul2708.common.report.Report;
import de.paul2708.common.util.Util;
import io.netty.buffer.ByteBuf;

import java.util.ArrayList;

/**
 * Created by Paul on 17.08.2016.
 */
@PacketInfo(18)
public class ListReportPacket extends Packet {

    private String name;
    private ArrayList<Report> reports;

    public ListReportPacket() { }

    public ListReportPacket(String name, ArrayList<Report> reports) {
        this.name = name;
        this.reports = reports;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.name = readString(byteBuf);
        this.reports = Util.readList(this, byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, name);
        Util.writeList(this, byteBuf, reports);
    }

    public String getName() {
        return name;
    }

    public ArrayList<Report> getReports() {
        return reports;
    }
}
