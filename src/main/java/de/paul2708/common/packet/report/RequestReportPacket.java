package de.paul2708.common.packet.report;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.jackwhite20.cascade.shared.protocol.packet.PacketInfo;
import io.netty.buffer.ByteBuf;

/**
 * Created by Paul on 31.08.2016.
 */
@PacketInfo(20)
public class RequestReportPacket  extends Packet {

    private String reporterName;
    private String targetName;
    private String targetServer;

    public RequestReportPacket() { }

    public RequestReportPacket(String reporterName, String targetName, String targetServer) {
        this.reporterName = reporterName;
        this.targetName = targetName;
        this.targetServer = targetServer;
    }

    @Override
    public void read(ByteBuf byteBuf) throws Exception {
        this.reporterName = readString(byteBuf);
        this.targetName = readString(byteBuf);
        this.targetServer = readString(byteBuf);
    }

    @Override
    public void write(ByteBuf byteBuf) throws Exception {
        writeString(byteBuf, reporterName);
        writeString(byteBuf, targetName);
        writeString(byteBuf, targetServer);
    }

    public String getReporterName() {
        return reporterName;
    }

    public String getTargetName() {
        return targetName;
    }

    public String getTargetServer() {
        return targetServer;
    }
}
