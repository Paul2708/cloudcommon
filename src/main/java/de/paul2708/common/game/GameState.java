package de.paul2708.common.game;

/**
 * Created by Paul on 11.01.2017.
 */
public enum GameState {

    NONE(0, 0, "-/-"),
    SETUP(1, 9, "Setup"),
    LOBBY(2, 5, "Lobby"),
    PREMIUM_ONLY(3, 1, "Premium"),
    INGAME(4, 14, "Ingame"),
    ;

    private int id;
    private int blockId;
    private String name;

    GameState(int id, int blockId, String name) {
        this.id = id;
        this.blockId = blockId;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public int getBlockId() {
        return blockId;
    }

    public String getName() {
        return name;
    }

    public static GameState getById(int id) {
        for (GameState gameState : values()) {
            if (gameState.getId() == id) {
                return gameState;
            }
        }

        return null;
    }
}
