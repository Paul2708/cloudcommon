package de.paul2708.common.achievement;

import java.io.Serializable;

/**
 * Created by Paul on 10.03.2017.
 */
public class Achievement implements Serializable {

    private int id;
    private String name;
    private String description;
    private int value;
    private int itemId;

    public Achievement() { }

    public Achievement(int id, String name, String description, int value, int itemId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.value = value;
        this.itemId = itemId;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getValue() {
        return value;
    }

    public int getItemId() {
        return itemId;
    }
}
