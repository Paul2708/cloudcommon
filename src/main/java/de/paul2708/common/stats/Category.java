package de.paul2708.common.stats;

import java.io.Serializable;

/**
 * Created by Paul on 16.10.2016.
 */
public class Category implements Serializable {

	private String name;
	private String dataName;
	private Type type;
	private int size;
	private String defaultValue;

	public Category(String name, String dataName, Type type, int size, String defaultValue) {
		this.name = name;
		this.dataName = dataName;
		this.type = type;
		this.size = size;
		this.defaultValue = defaultValue;
	}

	public String getName() {
		return name;
	}

	public String getDataName() {
		return dataName;
	}

	public Type getType() {
		return type;
	}

	public int getSize() {
		return size;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public enum Type {

		INTEGER("integer", Integer.class),
		VARCHAR("varchar", String.class),
		LONG("long", Long.class)
		;
		
		private String name;
		private Class clazz;

		Type(String name, Class<?> clazz) {
			this.name = name;
			this.clazz = clazz;
		}
		
		public String getName() {
			return name;
		}

		public Class getClazz() {
			return clazz;
		}

		public <T> T get(String data) {
			if (this == INTEGER) {
				return (T) Integer.valueOf(data);
			} else if (this == VARCHAR) {
				return (T) data;
			} else if (this == LONG) {
				return (T) Long.valueOf(data);
			}

			return null;
		}
	}
}
