package de.paul2708.common.stats;

import de.paul2708.common.util.Util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Paul on 22.10.2016.
 */
public class StatisticData implements Serializable {

    private UUID uuid;
    private String game;
    private Map<Category, Object> stats;

    public StatisticData(UUID uuid, String game) {
        this.uuid = uuid;
        this.game = game;

        this.stats = new HashMap();
    }

    public void set(String key, Object value) {
        stats.put(getCategory(key), value);
    }

    public <T> T get(String key) {
        return Util.getValue(getCategory(key), stats.get(getCategory(key)));
    }

    public void setStats(Map<Category, Object> stats) {
        this.stats = stats;
    }

    private Category getCategory(String key) {
        for (Category category : stats.keySet()) {
            if (category.getName().equalsIgnoreCase(key)) {
                return category;
            }
        }

        return null;
    }

    public String getGame() {
        return game;
    }

    public UUID getUuid() {
        return uuid;
    }

    public Map<Category, Object> getStats() {
        return stats;
    }
}
