package de.paul2708.common.stats;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Paul on 23.10.2016.
 */
public class GameStatistic implements Serializable {

    private String name;
    private List<Category> categories;
    private String serverTag;

    public GameStatistic(String name, List<Category> categories, String serverTag) {
        this.name = name;
        this.categories = categories;
        this.serverTag = serverTag;
    }

    public String getName() {
        return name;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public String getServerTag() {
        return serverTag;
    }
}
