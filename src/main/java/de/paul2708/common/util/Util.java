package de.paul2708.common.util;

import de.jackwhite20.cascade.shared.protocol.packet.Packet;
import de.paul2708.common.api.Information;
import de.paul2708.common.api.Update;
import de.paul2708.common.stats.Category;
import io.netty.buffer.ByteBuf;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Paul on 07.10.2016.
 */
public class Util {

    // Packet
    public static <T> ArrayList<T> readList(Packet packet, ByteBuf byteBuf) throws Exception {
        ArrayList<T> list = new ArrayList<>();
        int length = byteBuf.readInt();

        for (int i = 0; i < length; i++) {
            list.add(packet.readObject(byteBuf));
        }

        return list;
    }

    public static <T> void writeList(Packet packet, ByteBuf byteBuf, List<T> list) throws Exception {
        int length = list.size();

        byteBuf.writeInt(length);

        for (T aList : list) {
            packet.writeObject(byteBuf, aList);
        }
    }

    public static ArrayList<UUID> readListUUID(Packet packet, ByteBuf byteBuf) {
        ArrayList<UUID> list = new ArrayList<>();
        int length = byteBuf.readInt();

        for (int i = 0; i < length; i++) {
            list.add(packet.readUUID(byteBuf));
        }

        return list;
    }

    public static void writeListUUID(Packet packet, ByteBuf byteBuf, ArrayList<UUID> list) {
        int length = list.size();

        byteBuf.writeInt(length);

        for (UUID aList : list) {
            packet.writeUUID(byteBuf, aList);
        }
    }

    public static Information[] readInformationArray(Packet packet, ByteBuf byteBuf) throws Exception {
        int length = byteBuf.readInt();
        Information[] array = new Information[length];

        for (int i = 0; i < length; i++) {
            array[i] = packet.readObject(byteBuf);
        }

        return array;
    }

    public static void writeInformationArray(Packet packet, ByteBuf byteBuf, Information[] array) throws Exception {
        int length = array.length;

        byteBuf.writeInt(length);

        for (Object object : array) {
            packet.writeObject(byteBuf, object);
        }
    }

    public static Update[] readUpdateArray(Packet packet, ByteBuf byteBuf) throws Exception {
        int length = byteBuf.readInt();
        Update[] array = new Update[length];

        for (int i = 0; i < length; i++) {
            array[i] = packet.readObject(byteBuf);
        }

        return array;
    }

    public static void writeUpdateArray(Packet packet, ByteBuf byteBuf, Update[] array) throws Exception {
        int length = array.length;

        byteBuf.writeInt(length);

        for (Object object : array) {
            packet.writeObject(byteBuf, object);
        }
    }

    // Stats
    public static <T> T getValue(Category category, Object object) {
        if (category.getType() == Category.Type.INTEGER) {
            return (T) Integer.valueOf((Integer) object);
        } else if (category.getType() == Category.Type.VARCHAR) {
            return (T) String.valueOf(object);
        } else if (category.getType() == Category.Type.LONG) {
            return (T) Long.valueOf((long) object);
        }

        return null;
    }

    public static <T> T getValueByString(Category category, String value) {
        if (category.getType() == Category.Type.INTEGER) {
            return (T) Integer.valueOf(value);
        } else if (category.getType() == Category.Type.VARCHAR) {
            return (T) value;
        } else if (category.getType() == Category.Type.LONG) {
            return (T) Long.valueOf(value);
        }

        return null;
    }

    public static <T> T convertInstanceOfObject(Object o, Class<T> clazz) {
        try {
            return clazz.cast(o);
        } catch(ClassCastException e) {
            return null;
        }
    }

    // Formal
    public static boolean isInt(String input) {
        try {
            Integer.valueOf(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isUUID(String input) {
        try {
            UUID.fromString(input);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
