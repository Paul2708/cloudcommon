package de.paul2708.common.file;

import de.paul2708.common.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Paul on 28.04.2017.
 */
public class Element<T> {

    private String key;
    private T value;

    public Element(String key, T value) {
        this.key = key;
        this.value = value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public String save() {
        String line = key + ": " + serialize(value);
        return line;
    }

    public String getKey() {
        return key;
    }

    public T getValue() {
        return value;
    }

    public static <K> String serialize(K value) {
        String line;

        if (value instanceof String) {
            line = "'" + value + "'";
        } else if (value instanceof Integer) {
            line = value + "";
        } else if (value instanceof Long) {
            line = value + "l";
        } else if (value instanceof Double) {
            line = value + "d";
        } else if (value instanceof Float) {
            line = value + "f";
        } else if (value instanceof Boolean) {
            line = value + "";
        } else if (value instanceof UUID) {
            line = value.toString();
        } else if (value instanceof List<?>) {
            if (((List) value).size() == 0) {
                line = "{ }";
            } else {
                line = "\n  ";
                for (int i = 0; i < ((List) value).size(); i++) {
                    line += "- " + serialize(((List) value).get(i)) + "\n  ";
                }
                line = line.substring(0, line.length() - 3);
            }
        } else {
            throw new IllegalArgumentException("serialize " + value.getClass().getSimpleName() + " is not supported yet");
        }

        return line;
    }

    public static <K> K deserialize(String input) {
        Object value;

        if (input.startsWith("'") && input.endsWith("'")) {
            value = input.substring(1, input.length() - 1);
        } else if (Util.isInt(input)) {
            value = Integer.valueOf(input);
        } else if (input.endsWith("l") || input.endsWith("L")) {
            value = Long.valueOf(input.substring(0, input.length() - 1));
        } else if (input.endsWith("d") || input.endsWith("D")) {
            value = Double.valueOf(input.substring(0, input.length() - 1));
        } else if (input.endsWith("f") || input.endsWith("F")) {
            value = Float.valueOf(input.substring(0, input.length() - 1));
        } else if (input.equalsIgnoreCase("true") || input.equalsIgnoreCase("false")) {
            value = Boolean.valueOf(input);
        } else if (Util.isUUID(input)) {
            value = UUID.fromString(input);
        } else if (input.equalsIgnoreCase("{ }")) {
            value = new ArrayList<>();
        } else if (input.startsWith("\n")) {
            List<?> list = new ArrayList<>();
            String[] array = input.split("\n- ");

            for (int i = 1; i < array.length; i++) {
                String line = array[i];

                if (line.endsWith("\n")) {
                    line = line.substring(0, line.length() - 1);
                }

                list.add(deserialize(line));
            }

            value = list;
        } else {
            throw new IllegalArgumentException("deserialize " + input + " is not supported yet");
        }

        return (K) value;
    }
}
