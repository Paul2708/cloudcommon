package de.paul2708.common.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Paul on 07.06.2016.
 */
public abstract class BasicFile {

    // TODO: comment between to keys causes error

    private String pathName;

    private Path path;
    private File file;

    private Map<Integer, Element<?>> values;

    public BasicFile(String path) {
        this.pathName = path;

        this.values = new ConcurrentHashMap<>();

        load();
    }

    private void load() {
        this.path = Paths.get(this.pathName);
        this.file = path.toFile();

        if (file == null || !file.exists()) {
            try {
                if (path.getParent() != null) {
                    Files.createDirectories(path.getParent());
                }

                Files.createFile(path);
                System.out.println("Datei '" + getFileName() + "' wurde erfolgreich erstellt.");
            } catch (IOException e) {
                System.err.println("Datei '" + getFileName() + "' konnte nicht erstellt werden. (" + e.getMessage() + ")");
                e.printStackTrace();
            }

            createDefaultValue();
        } else {
            reload();
        }
    }

    public abstract void createDefaultValue();

    public void comment(String comment) {
        Element<String> element = new Element<>("#comment", comment);
        this.values.put(values.size(), element);
    }

    public <T> void set(String key, T value) {
        Element element = getElement(key);

        if (element == null) {
            element = new Element<>(key, value);
            this.values.put(values.size(), element);
        } else {
            if (value == null) {
                values.remove(getIndex(key));
            } else {
                element.setValue(value);
            }
        }
    }

    public <T> T get(String key) {
        Element<?> element = getElement(key);

        if (element == null) {
            throw new IllegalArgumentException("key " + key + " doesn't exist");
        } else {
            return (T) element.getValue();
        }
    }

    public void reload() {
        this.values.clear();

        List<String> lines = resolveLines();
        List<String> parsed = parseKeyAndValues(lines);

        for (String keyAndValue : parsed) {
            String key = keyAndValue.split(": ")[0];
            String value = keyAndValue.substring(key.length() + 2, keyAndValue.length());

            Element<?> element = new Element<>(key, Element.deserialize(value));
            this.values.put(values.size(), element);
        }
    }

    public void save() {
        List<Integer> sortedKeys = new ArrayList<>(values.keySet());
        Collections.sort(sortedKeys);

        String content = "";
        for (int index : sortedKeys) {
            Element<?> element = values.get(index);

            if (element.getKey().equalsIgnoreCase("#comment")) {
                content += "# " + element.getValue();
            } else {
                content += element.getKey() + ": " + Element.serialize(element.getValue());
            }

            content += "\n";
        }

        content = content.substring(0, content.length() - 1);

        try {
            Files.write(path, content.getBytes("utf-8"));
        } catch (IOException e) {
            System.err.println("Datei '" + getFileName() + "' konnte nicht beschrieben werden. (" + e.getMessage() + ")");
            e.printStackTrace();
        }
    }

    public void delete() {
        if (this.file.delete()) {
            System.out.println("Datei '" + getFileName() + "' wurde erfolgreich gelöscht.");
        } else {
            System.err.println("Datei '" + getFileName() + "' konnte nicht gelöscht werden.");
        }
    }

    public List<String> resolveLines() {
        List<String> list = new ArrayList<>();
        try {
            for (String line : Files.readAllLines(path)) {
                line = line.trim();
                if (line.trim().equalsIgnoreCase("")) {
                    continue;
                }

                list.add(line);
            }
        } catch (Exception e) {
            System.err.println("Inhalt der Datei '" + getFileName() + "' konnte nicht geladen werden. (" + e.getMessage() + ")");
            e.printStackTrace();
        }

        return list;
    }

    private String getFileName() {
        String[] array = this.pathName.split("/");
        return array[array.length - 1];
    }

    private Element<?> getElement(String key) {
        for (Element<?> element : values.values()) {
            if (element.getKey().equalsIgnoreCase(key)) {
                return element;
            }
        }

        return null;
    }

    private boolean hasKey(String line) {
        line = line.trim();
        return !line.startsWith(" ") && !line.startsWith("#") && line.contains(":") &&
                !line.startsWith("-");
    }

    private List<String> parseKeyAndValues(List<String> lines) {
        List<String> parsed = new ArrayList<>();

        for (int i = 0; i < lines.size(); i++) {
            String line = lines.get(i);
            String current;
            boolean first;

            if (hasKey(line)) {
                current = line;
                first = true;

                for (int j = i + 1; j < lines.size(); j++) {
                    String next = lines.get(j);
                    if (!hasKey(next)) {
                        if (first) {
                            current += " \n";
                            first = false;
                        }

                        current += next + "\n";
                    } else {
                        break;
                    }
                }

                if (current.endsWith("\n")) {
                    current = current.substring(0, current.length() - 1);
                }

                parsed.add(current);
            }
        }

        return parsed;
    }
    
    private int getIndex(String key) {
        for (Map.Entry<Integer, Element<?>> entry : values.entrySet()) {
            if (entry.getValue().getKey().equalsIgnoreCase(key)) {
                return entry.getKey();
            }
        }

        return -1;
    }
}