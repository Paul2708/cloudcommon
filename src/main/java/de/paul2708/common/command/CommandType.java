package de.paul2708.common.command;

/**
 * Created by Paul on 08.08.2016.
 */
public enum CommandType {

    // TODO: /help command with permission and description and aliases and auto complete
    // TODO: Event command

    RANK("rank"),
    SET_RANK("setrank"),
    PERMISSION("permission"),
    REPORT("report"),
    REPORTS("reports"),
    BROADCAST("broadcast"),
    SEND("send"),
    WHEREAMI("whereami"),
    SENDALL("sendall"),
    GOTO("goto"),
    FIND("find"),
    TEAMCHAT("tc"),
    ADMINCHAT("ac"),
    ONLINE("online"),
    TEAM("team"),
    LOADMESSAGES("loadmessages"),
    SETMOTD("setmotd"),
    CHATLOG("chatlog"),
    UNLINK("unlink"),
    TS("ts"),
    BUILDEVENT("bauevent"),
    BUILDSERVER("bauserver"),
    BLACKLIST("blacklist"),
    MAINTENANCE("maintenance"),
    INFO("info"),
    JOIN("join"),
    BAN("ban"),
    MUTE("mute"),
    KICK("kick"),
    UNMUTE("unmute"),
    UNBAN("unban"), PARDON("pardon"),
    WARN("warn"),
    PLAYTIME("playtime"),
    TOKENS("tokens"),
    STATS("stats"),
    SERVER("server"),
    LOBBY("lobby"), HUB("hub"), LEAVE("leave"), L("l"),
    NOTIFICATION("notification"),
    TWEET("tweet"),
    CLOUD("cloud"),
    TEMPLATE("template"), TEMPLATES("templates"),
    CONNECTION("connection"),
    KEY("key"),
    BUG("bug"),
    MSG("msg"),
    REPLY("reply"), R("r"),
    SETMAXPLAYERS("setmaxplayers"),
    LOG("log"),
    NICK("nick"), UNNICK("unnick"),
    SURVEY("survey"),
    ;

    private String command;

    CommandType(String command) {
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    public static CommandType getTypeByCommand(String command) {
        for (CommandType type : CommandType.values()) {
            if (type.getCommand().equalsIgnoreCase(command)) {
                return type;
            }
        }

        return null;
    }
}
