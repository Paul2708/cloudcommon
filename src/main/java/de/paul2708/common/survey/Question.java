package de.paul2708.common.survey;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsonable;

import java.io.IOException;
import java.io.Serializable;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul on 17.06.2017.
 */
public class Question implements Serializable, Jsonable {

    private String question;
    private Map<String, Integer> answers;

    public Question(String question, Map<String, Integer> answers) {
        this.question = question;
        this.answers = answers;
    }

    public void clear() {
        Collection<String> collection = answers.keySet();

        for (String answer : collection) {
            answers.put(answer, 0);
        }
    }

    public void setVotes(String answer, int total) {
        answers.put(answer, total);
    }

    public String getQuestion() {
        return question;
    }

    public int getVotes(String answer) {
        return answers.get(answer);
    }

    public Map<String, Integer> getAnswers() {
        return answers;
    }

    @Override
    public String toJson() {
        JsonObject object = new JsonObject();
        object.put("question", question);

        JsonArray array = new JsonArray();

        for (String single : answers.keySet()) {
            JsonObject answer = new JsonObject();

            answer.put("answer", single);
            answer.put("votes", answers.get(single));

            array.add(answer);
        }

        object.put("answers", array);

        return object.toJson();
    }

    @Override
    public void toJson(Writer writer) throws IOException {
        writer.write(toJson());
        writer.flush();
    }

    public static Question build(JsonObject object) {
        String question = object.getString("question");
        Map<String, Integer> result = new HashMap<>();

        JsonArray array = (JsonArray) object.get("answers");

        for (int i = 0; i < array.size(); i++) {
            JsonObject answerObject = (JsonObject) array.get(i);
            String answer = answerObject.getString("answer");
            int votes = answerObject.getInteger("votes");

            result.put(answer, votes);
        }

        return new Question(question, result);
    }
}
