package de.paul2708.common.survey;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Paul on 26.06.2017.
 */
public class Result implements Serializable {

    private Map<Integer, String> result;

    public Result() {
        this.result = new HashMap<>();
    }

    public void setVote(int question, String answer) {
        this.result.put(question, answer);
    }

    public Map<Integer, String> getResult() {
        return result;
    }
}
