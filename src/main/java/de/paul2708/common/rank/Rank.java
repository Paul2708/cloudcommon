package de.paul2708.common.rank;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 08.08.2016.
 */
public class Rank implements Serializable {

    private String name;
    private String tag;
    private String shortTag;
    private Integer permissionLevel;
    private String permission;
    private Integer teamspeakGroudId;

    private List<String> permissionList;

    public Rank(String name, String tag, String shortTag, Integer permissionLevel, String permission, Integer teamspeakGroudId) {
        this.name = name.replaceAll("&", "§");
        this.name = name.replaceAll("&", "§");
        this.tag = tag.replaceAll("&", "§");
        this.shortTag = shortTag.replaceAll("&", "§");
        this.permissionLevel = permissionLevel;
        this.permission = permission;
        this.teamspeakGroudId = teamspeakGroudId;
    }

    public String getName() {
        return name;
    }

    public String getPrefix() {
        if (tag.startsWith("§")) {
            return tag.substring(0, 2);
        } else {
            return "";
        }
    }

    public String getTag() {
        return tag;
    }

    public String getShortTag() {
        return shortTag;
    }

    public Integer getPermissionLevel() {
        return permissionLevel;
    }

    public List<String> getPermissionList() {
        if (permissionList == null) {
            this.permissionList = new CopyOnWriteArrayList<>();
            if (!permission.equalsIgnoreCase("")) {
                Collections.addAll(this.permissionList, permission.split(";"));
            }
        }

        return permissionList;
    }

    public void addPermission(String aPermission) {
        getPermissionList();

        this.permissionList.add(aPermission);
        this.permission = "";

        for (String perm : permissionList) {
            this.permission += perm + ";";
        }

        this.permission = this.permission.substring(0, this.permission.length() - 1);
    }

    public void removePermission(String aPermission) {
        getPermissionList();

        this.permissionList.remove(aPermission);
        this.permission = "";

        for (String perm : permissionList) {
            this.permission += perm + ";";
        }

        if (permission.endsWith("\\;")) {
            this.permission = this.permission.substring(0, this.permission.length() - 1);
        }
    }

    public String getPermission() {
        return permission;
    }

    public Integer getTeamspeakGroudId() {
        return teamspeakGroudId;
    }

    @Override
    public String toString() {
        return "Rank{" +
                "name='" + name + '\'' +
                ", tag='" + tag + '\'' +
                ", shortTag='" + shortTag + '\'' +
                ", permissionLevel=" + permissionLevel +
                ", permission='" + permission + '\'' +
                ", teamspeakGroudId='" + teamspeakGroudId + "'\'" +
                '}';
    }
}
