import de.paul2708.common.file.BasicFile;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * Created by Paul on 01.02.2017.
 */
public class FileExample extends BasicFile {

    public FileExample() {
        super("./src/test/java/test.yml");
    }

    @Override
    public void createDefaultValue() {

        comment("Kommentar für die Config");

        // Supported types
        set("boolean", true);
        set("string", "Name");
        set("int", 20);
        set("double", 0.25D);
        set("long", System.currentTimeMillis());
        set("float", 0.1545120f);
        set("uuid", UUID.fromString("bc3b3f26-e70c-4c66-bdec-5bd7246967bc"));
        set("list", Arrays.asList("Tegrgrthrgh", "rfgaedrfgwwfTHRWGH"));

        set("key", "sample value");

        save();
    }

    public static void main(String[] args) {
        FileExample fileExample = new FileExample();

        // Get values by key
        boolean setup = fileExample.get("boolean");
        String name = fileExample.get("string");
        int number = fileExample.get("int");
        double damage = fileExample.get("double");
        long timeStamp = fileExample.get("long");
        float factor = fileExample.get("float");
        UUID uuid = fileExample.get("uuid");
        List<String> list = fileExample.get("list");

        // Remove key and value
        /*fileExample.set("key", null);
        fileExample.save();

        // Change values
        fileExample.set("key", "new value");
        fileExample.save();*/
    }
}
